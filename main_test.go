package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"gitlab.com/haibin/weather/openweather"
)

type weatherMock struct {
	city string
	temp float64
	err  error
}

func (w weatherMock) Temp(city string) (string, float64, error) {
	return w.city, w.temp, w.err
}

func TestTempOK(t *testing.T) {
	dbUser := os.Getenv("DB_USER")
	dbPassword := os.Getenv("DB_PASSWORD")
	dbHost := os.Getenv("DB_HOST")
	dbName := os.Getenv("DB_NAME")
	db, err := GetDB(dbUser, dbPassword, dbHost, dbName)
	if err != nil {
		t.Fatal(err)
	}

	expected := 29.9
	e := Env{
		Weather: weatherMock{temp: expected},
		DB:      db,
	}

	req := httptest.NewRequest("GET", "/temp?city=singapore", nil)
	w := httptest.NewRecorder()
	e.ServeHTTP(w, req)

	resp := w.Result()
	body, _ := ioutil.ReadAll(resp.Body)

	if resp.StatusCode != http.StatusOK {
		t.Errorf("got %d, want %d", resp.StatusCode, http.StatusOK)
	}

	var m map[string]interface{}
	json.Unmarshal(body, &m)

	temp := m["temp"].(float64)
	if temp != expected {
		t.Errorf("got %f, want %f", temp, expected)
	}
}

func TestTempNotFound(t *testing.T) {
	dbUser := os.Getenv("DB_USER")
	dbPassword := os.Getenv("DB_PASSWORD")
	dbHost := os.Getenv("DB_HOST")
	dbName := os.Getenv("DB_NAME")
	db, err := GetDB(dbUser, dbPassword, dbHost, dbName)
	if err != nil {
		t.Fatal(err)
	}

	expected := 29.9
	e := Env{
		Weather: weatherMock{
			temp: expected,
			err:  openweather.ErrNotFound,
		},
		DB: db,
	}

	req := httptest.NewRequest("GET", "/temp?city=singapore1", nil)
	w := httptest.NewRecorder()
	e.ServeHTTP(w, req)

	resp := w.Result()

	if resp.StatusCode != http.StatusNotFound {
		t.Errorf("got %d, want %d", resp.StatusCode, http.StatusOK)
	}
}

func TestTempMissingParam(t *testing.T) {
	e := Env{}

	req := httptest.NewRequest("GET", "/temp", nil)
	w := httptest.NewRecorder()
	e.ServeHTTP(w, req)

	resp := w.Result()

	if resp.StatusCode != http.StatusBadRequest {
		t.Errorf("got %d, want %d", resp.StatusCode, http.StatusOK)
	}
}
