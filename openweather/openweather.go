package openweather

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"net/url"
)

// ErrNotFound returns if the city is not found.
var ErrNotFound = errors.New("not found")

type Openweather struct {
	URL   string
	AppID string
}

// New returns
func New(weatherURL, appid string) Openweather {
	return Openweather{
		URL:   weatherURL,
		AppID: appid,
	}
}

// Temp returns the temperature for the city.
func (o Openweather) Temp(city string) (string, float64, error) {
	wURL, _ := url.Parse(o.URL + "/data/2.5/weather")
	q := wURL.Query()
	q.Add("q", city)
	q.Add("appid", o.AppID)
	wURL.RawQuery = q.Encode()

	resp, err := http.Get(wURL.String())
	if err != nil {
		return "", 0, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", 0, err
	}

	if resp.StatusCode == http.StatusNotFound {
		return "", 0, ErrNotFound
	}
	if resp.StatusCode != http.StatusOK {
		return "", 0, errors.New(string(body))
	}

	var openWatherResp struct {
		Main struct {
			Temp float64 `json:"temp"`
		} `json:"main"`
		Name string `json:"name"`
	}
	err = json.Unmarshal(body, &openWatherResp)
	if err != nil {
		return "", 0, err
	}

	return openWatherResp.Name, openWatherResp.Main.Temp, nil
}
