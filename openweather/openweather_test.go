package openweather_test

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/haibin/weather/openweather"
)

func TestTempOK(t *testing.T) {
	expectedTemp := 304.17
	expectedCity := "Singapore"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, `{
			"coord": {
				"lon": 103.85,
				"lat": 1.29
			},
			"weather": [
				{
					"id": 803,
					"main": "Clouds",
					"description": "broken clouds",
					"icon": "04d"
				}
			],
			"base": "stations",
			"main": {
				"temp": %f,
				"pressure": 1007,
				"humidity": 70,
				"temp_min": 303.15,
				"temp_max": 305.15
			},
			"visibility": 10000,
			"wind": {
				"speed": 2.6,
				"deg": 40
			},
			"clouds": {
				"all": 75
			},
			"dt": 1516858200,
			"sys": {
				"type": 1,
				"id": 8146,
				"message": 0.0064,
				"country": "MY",
				"sunrise": 1516835706,
				"sunset": 1516879123
			},
			"id": 1880252,
			"name": "%s",
			"cod": 200
		}`, expectedTemp, expectedCity)
	}))
	defer ts.Close()

	ow := openweather.New(ts.URL, "appid123")
	city, temp, err := ow.Temp("singapore")
	if err != nil {
		t.Error(err)
	}
	if temp != expectedTemp {
		t.Errorf("got %f, want %f", temp, expectedTemp)
	}
	if city != expectedCity {
		t.Errorf("got %s, want %s", city, expectedCity)
	}
}

func TestTempNotFound(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusNotFound)
	}))
	defer ts.Close()

	ow := openweather.New(ts.URL, "appid123")
	_, _, err := ow.Temp("singapore1")
	if err != openweather.ErrNotFound {
		t.Errorf("no error, want %s", openweather.ErrNotFound)
	}
}
