
# Weather

## Local

```
> docker run -p 5432:5432 --name jarvis-postgres -e POSTGRES_PASSWORD=mysecretpassword -d postgres:latest
> env PGPASSWORD=mysecretpassword createdb -h localhost -p 5432 -U postgres weather
> env \
    DB_USER=postgres \
    DB_PASSWORD=mysecretpassword \
    DB_HOST=localhost \
    DB_NAME=weather \
    goose -env test up
```

```
> go build
> env APPID=xxx \
    DB_USER=postgres \
    DB_PASSWORD=mysecretpassword \
    DB_HOST=localhost \
    DB_NAME=weather \
    ./weather
```

```
> curl --request GET --url 'http://localhost:8080/temp?city=sin'
{"city":"Singapore","temp":301.22}
```

```
> curl --request GET --url 'http://localhost:8080/temp?city=singapore'
{"city":"Singapore","temp":301.22}
```

## Test

```
> env \
    DB_USER=postgres \
    DB_PASSWORD=mysecretpassword \
    DB_HOST=localhost \
    DB_NAME=weather \
    goose -env test up
> env \
    DB_USER=postgres \
    DB_PASSWORD=mysecretpassword \
    DB_HOST=localhost \
    DB_NAME=weather \
    go test
```

## Reference
http://txt.fliglio.com/2014/12/testing-microservices-in-go/