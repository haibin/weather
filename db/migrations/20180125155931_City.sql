
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE city (
    id serial PRIMARY KEY,
    short_name text NOT NULL,
    name text NOT NULL
);

INSERT INTO city (short_name, name) VALUES ('sin', 'singapore');


-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back

DROP TABLE city;
