package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"

	_ "github.com/lib/pq"
	"gitlab.com/haibin/weather/openweather"
)

type Weather interface {
	Temp(city string) (string, float64, error)
}

type Env struct {
	Weather Weather
	DB      *sql.DB
}

func (e Env) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	city := r.URL.Query().Get("city")
	if city == "" {
		http.Error(w, "", http.StatusBadRequest)
		return
	}

	// city might be a short name so we query the DB for the name.
	var cityName string
	err := e.DB.QueryRow("select name from city where short_name = $1", city).Scan(&cityName)
	if err != nil && err != sql.ErrNoRows {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if err == sql.ErrNoRows {
		cityName = city
	}

	name, temp, err := e.Weather.Temp(cityName)
	if err == openweather.ErrNotFound {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	resp := struct {
		City string  `json:"city"`
		Temp float64 `json:"temp"`
	}{
		City: name,
		Temp: temp,
	}
	data, _ := json.Marshal(resp)
	w.Write(data)
}

func main() {
	appid := os.Getenv("APPID")
	if appid == "" {
		log.Fatal("APPID is required.")
	}

	dbUsername := os.Getenv("DB_USER")
	dbPassword := os.Getenv("DB_PASSWORD")
	dbHost := os.Getenv("DB_HOST")
	dbName := os.Getenv("DB_NAME")
	db, err := sql.Open("postgres", fmt.Sprintf("postgres://%s:%s@%s/%s?sslmode=disable",
		dbUsername, dbPassword, dbHost, dbName))
	if err != nil {
		log.Fatal(err)
	}

	if err = db.Ping(); err != nil {
		log.Fatal("DB ping failed")
	}

	env := Env{
		Weather: openweather.New("http://api.openweathermap.org", appid),
		DB:      db,
	}

	http.Handle("/temp", env)

	log.Fatal(http.ListenAndServe(":8080", nil))
}

func GetDB(username, password, host, dbname string) (*sql.DB, error) {
	db, err := sql.Open("postgres", fmt.Sprintf("postgres://%s:%s@%s/%s?sslmode=disable",
		username, password, host, dbname))
	if err != nil {
		return nil, err
	}

	if err = db.Ping(); err != nil {
		return nil, err
	}

	return db, nil
}
